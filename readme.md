SpringMVC 中整合JSON、XML视图一

http://www.cnblogs.com/hoojo/archive/2011/04/29/2032571.html

SpringMVC 中整合JSON、XML视图二

http://www.cnblogs.com/hoojo/archive/2011/04/29/2032609.html

----------
## 七、 JSON-lib转换Java到JSON ##
http://localhost:8080/spring-mvc-json-xml-demo/jsonlib/view/doBeanJsonView

 
    {
		"account": {
			"address": "北京",
			"birthday": {
				"birthday": "2010-11-22"
			},
			"email": "email",
			"id": 1,
			"name": "haha"
			}
	}

http://localhost:8080/spring-mvc-json-xml-demo/jsonlib/view/doMapJsonView

	{
	    "hashMap": {
	        "success": true,
	        "user": {
	            "address": "china GuangZhou",
	            "age": 23,
	            "birthday": {
	                "date": 10,
	                "day": 1,
	                "hours": 14,
	                "minutes": 1,
	                "month": 7,
	                "seconds": 39,
	                "time": 1439186499277,
	                "timezoneOffset": -480,
	                "year": 115
	            },
	            "name": "jack",
	            "sex": true
	        }
	    },
	    "title": "ViewController doBeanJsonView"
	}

http://localhost:8080/spring-mvc-json-xml-demo/jsonlib/view/doListJsonView

	{
	    "userList": [
	        {
	            "address": "china GuangZhou#0",
	            "age": 23,
	            "birthday": {
	                "date": 10,
	                "day": 1,
	                "hours": 14,
	                "minutes": 4,
	                "month": 7,
	                "seconds": 6,
	                "time": 1439186646673,
	                "timezoneOffset": -480,
	                "year": 115
	            },
	            "name": "jack_0",
	            "sex": true
	        },
	        {
	            "address": "china GuangZhou#1",
	            "age": 24,
	            "birthday": {
	                "date": 10,
	                "day": 1,
	                "hours": 14,
	                "minutes": 4,
	                "month": 7,
	                "seconds": 6,
	                "time": 1439186646673,
	                "timezoneOffset": -480,
	                "year": 115
	            },
	            "name": "jack_1",
	            "sex": true
	        },
	        {
	            "address": "china GuangZhou#2",
	            "age": 25,
	            "birthday": {
	                "date": 10,
	                "day": 1,
	                "hours": 14,
	                "minutes": 4,
	                "month": 7,
	                "seconds": 6,
	                "time": 1439186646673,
	                "timezoneOffset": -480,
	                "year": 115
	            },
	            "name": "jack_2",
	            "sex": true
	        }
	    ]
	}

----------
## 六、 Jackson转换Java对象 ##

http://localhost:8080/spring-mvc-json-xml-demo/jackson/view/doBeanJsonView

	{
	  "account" : {
	    "id" : 1,
	    "name" : "haha",
	    "email" : "email",
	    "address" : "北京",
	    "birthday" : {
	      "birthday" : "2010-11-22"
	    }
	  }
	}

http://localhost:8080/spring-mvc-json-xml-demo/jackson/view/doMapJsonView

	{
	  "hashMap" : {
	    "success" : true,
	    "user" : {
	      "name" : "jack",
	      "age" : 23,
	      "sex" : true,
	      "address" : "china GuangZhou",
	      "birthday" : 1439182082530
	    }
	  },
	  "title" : "ViewController doBeanJsonView"
	}

http://localhost:8080/spring-mvc-json-xml-demo/jackson/view/doListJsonView

	{
	  "userList" : [
	    {
	      "name" : "jack_0",
	      "age" : 23,
	      "sex" : true,
	      "address" : "china GuangZhou#0",
	      "birthday" : 1439182315879
	    },
	    {
	      "name" : "jack_1",
	      "age" : 24,
	      "sex" : true,
	      "address" : "china GuangZhou#1",
	      "birthday" : 1439182315879
	    },
	    {
	      "name" : "jack_2",
	      "age" : 25,
	      "sex" : true,
	      "address" : "china GuangZhou#2",
	      "birthday" : 1439182315879
	    }
	  ]
	}

----------
## 四、 用Castor转换XML ##

http://localhost:8080/spring-mvc-json-xml-demo/castor/view/doArrayXMLCastorView

	<?xml version="1.0" encoding="UTF-8"?>
	<array>
	    <Account id="1"
	        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Account">
	        <name>haha#0</name>
	        <email>email0@120.com</email>
	        <address>address#0</address>
	        <生日 birthday="2010-11-20"/>
	    </Account>
	    <Account id="2"
	        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Account">
	        <name>haha#1</name>
	        <email>email1@121.com</email>
	        <address>address#1</address>
	        <生日 birthday="2010-11-21"/>
	    </Account>
	    <user age="23" xmlns:java="http://java.sun.com"
	        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="java:com.hoo.entity.User">
	        <address>china GuangZhou# </address>
	        <name>jack#</name>
	        <sex>true</sex>
	        <birthday>2015-08-07T14:52:27.120+08:00</birthday>
	    </user>
	</array>



http://localhost:8080/spring-mvc-json-xml-demo/castor/view/doListXMLCastorView

	<?xml version="1.0" encoding="UTF-8"?>
	<listBean>
	    <Account id="1">
	        <name>haha#0</name>
	        <email>email0@120.com</email>
	        <address>address#0</address>
	        <生日 birthday="2010-11-20"/>
	    </Account>
	    <user age="23" xmlns:java="http://java.sun.com"
	        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="java:com.hoo.entity.User">
	        <address>china GuangZhou# 0</address>
	        <name>jack#0</name>
	        <sex>false</sex>
	        <birthday>2015-08-07T14:48:12.615+08:00</birthday>
	    </user>
	    <Account id="2">
	        <name>haha#1</name>
	        <email>email1@121.com</email>
	        <address>address#1</address>
	        <生日 birthday="2010-11-21"/>
	    </Account>
	    <user age="24" xmlns:java="http://java.sun.com"
	        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="java:com.hoo.entity.User">
	        <address>china GuangZhou# 1</address>
	        <name>jack#1</name>
	        <sex>false</sex>
	        <birthday>2015-08-07T14:48:12.615+08:00</birthday>
	    </user>
	    <Account id="3">
	        <name>haha#2</name>
	        <email>email2@122.com</email>
	        <address>address#2</address>
	        <生日 birthday="2010-11-22"/>
	    </Account>
	    <user age="25" xmlns:java="http://java.sun.com"
	        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="java:com.hoo.entity.User">
	        <address>china GuangZhou# 2</address>
	        <name>jack#2</name>
	        <sex>false</sex>
	        <birthday>2015-08-07T14:48:12.615+08:00</birthday>
	    </user>
	</listBean>


http://localhost:8080/spring-mvc-json-xml-demo/castor/view/doMapXMLCastorView

	<?xml version="1.0" encoding="UTF-8"?>
	<map-bean>
	    <map key="account">
	        <Account id="1">
	            <name>haha</name>
	            <email>email</email>
	            <address>北京</address>
	            <生日 birthday="2010-11-22"/>
	        </Account>
	    </map>
	    <map key="day">
	        <birthday birthday="2010-11-22"/>
	    </map>
	</map-bean>


http://localhost:8080/spring-mvc-json-xml-demo/castor/view/doBeanXMLCastorView

	<?xml version="1.0" encoding="UTF-8"?>
	<Account id="1">
	    <name>haha</name>
	    <email>email</email>
	    <address>北京</address>
	    <生日 birthday="2010-11-22"/>
	</Account>


----------
## 三、 利用xStream转换XML ##

http://localhost:8080/spring-mvc-json-xml-demo/xStream/view/doMoreXMLXStream

	<myBeans-array>
	    <myBeans>
	        <id>1</id>
	        <name>haha</name>
	        <email>email</email>
	        <address>北京</address>
	        <birthday>
	            <birthday>2010-11-22</birthday>
	        </birthday>
	    </myBeans>
	    <myBeans>
	        <id>1</id>
	        <name>haha</name>
	        <email>email</email>
	        <address>上海</address>
	        <birthday>
	            <birthday>2010-11-22</birthday>
	        </birthday>
	    </myBeans>
	</myBeans-array>

http://localhost:8080/spring-mvc-json-xml-demo/xStream/view/doDifferXMLXStream

	<map>
	    <entry>
	        <string>bean</string>
	        <myBeans>
	            <id>1</id>
	            <name>haha</name>
	            <email>email</email>
	            <address>广东</address>
	            <birthday>
	                <birthday>2010-11-22</birthday>
	            </birthday>
	        </myBeans>
	    </entry>
	    <entry>
	        <string>user</string>
	        <com.hoo.entity.User>
	            <name>jack</name>
	            <age>23</age>
	            <sex>true</sex>
	            <address>china GuangZhou</address>
	            <birthday>2015-08-10 06:38:12.528 UTC</birthday>
	        </com.hoo.entity.User>
	    </entry>
	</map>

http://localhost:8080/spring-mvc-json-xml-demo/xStream/view/doListXMLXStream

	<list>
	    <myBeans>
	        <id>1</id>
	        <name>haha#0</name>
	        <email>email0@120.com</email>
	        <address>北京#0</address>
	        <birthday>
	            <birthday>2010-11-20</birthday>
	        </birthday>
	    </myBeans>
	    <com.hoo.entity.User>
	        <name>jack#0</name>
	        <age>23</age>
	        <sex>false</sex>
	        <address>china GuangZhou 广州# 0</address>
	        <birthday>2015-08-10 06:36:57.245 UTC</birthday>
	    </com.hoo.entity.User>
	    <myBeans>
	        <id>2</id>
	        <name>haha#1</name>
	        <email>email1@121.com</email>
	        <address>北京#1</address>
	        <birthday>
	            <birthday>2010-11-21</birthday>
	        </birthday>
	    </myBeans>
	    <com.hoo.entity.User>
	        <name>jack#1</name>
	        <age>24</age>
	        <sex>false</sex>
	        <address>china GuangZhou 广州# 1</address>
	        <birthday>2015-08-10 06:36:57.245 UTC</birthday>
	    </com.hoo.entity.User>
	    <myBeans>
	        <id>3</id>
	        <name>haha#2</name>
	        <email>email2@122.com</email>
	        <address>北京#2</address>
	        <birthday>
	            <birthday>2010-11-22</birthday>
	        </birthday>
	    </myBeans>
	    <com.hoo.entity.User>
	        <name>jack#2</name>
	        <age>25</age>
	        <sex>false</sex>
	        <address>china GuangZhou 广州# 2</address>
	        <birthday>2015-08-10 06:36:57.245 UTC</birthday>
	    </com.hoo.entity.User>
	</list>

----------
## 利用Jaxb2编组XML ##

http://localhost:8080/spring-mvc-json-xml-demo/jaxb2/view/doListXMLXStream

	<list>
	    <myBeans>
	        <id>1</id>
	        <name>haha#0</name>
	        <email>email0@120.com</email>
	        <address>北京#0</address>
	        <birthday>
	            <birthday>2010-11-20</birthday>
	        </birthday>
	    </myBeans>
	    <com.hoo.entity.User>
	        <address>china GuangZhou 广州# 0</address>
	        <age>23</age>
	        <birthday>2015-08-07 05:29:30.123 UTC</birthday>
	        <name>jack#0</name>
	        <sex>false</sex>
	    </com.hoo.entity.User>
	    <myBeans>
	        <id>2</id>
	        <name>haha#1</name>
	        <email>email1@121.com</email>
	        <address>北京#1</address>
	        <birthday>
	            <birthday>2010-11-21</birthday>
	        </birthday>
	    </myBeans>
	    <com.hoo.entity.User>
	        <address>china GuangZhou 广州# 1</address>
	        <age>24</age>
	        <birthday>2015-08-07 05:29:30.123 UTC</birthday>
	        <name>jack#1</name>
	        <sex>false</sex>
	    </com.hoo.entity.User>
	    <myBeans>
	        <id>3</id>
	        <name>haha#2</name>
	        <email>email2@122.com</email>
	        <address>北京#2</address>
	        <birthday>
	            <birthday>2010-11-22</birthday>
	        </birthday>
	    </myBeans>
	    <com.hoo.entity.User>
	        <address>china GuangZhou 广州# 2</address>
	        <age>25</age>
	        <birthday>2015-08-07 05:29:30.123 UTC</birthday>
	        <name>jack#2</name>
	        <sex>false</sex>
	    </com.hoo.entity.User>
	</list>


http://localhost:8080/spring-mvc-json-xml-demo/jaxb2/view/doDifferXMLXStream

	<map>
	    <entry>
	        <string>bean</string>
	        <myBeans>
	            <id>1</id>
	            <name>haha</name>
	            <email>email</email>
	            <address>广东</address>
	            <birthday>
	                <birthday>2010-11-22</birthday>
	            </birthday>
	        </myBeans>
	    </entry>
	    <entry>
	        <string>user</string>
	        <com.hoo.entity.User>
	            <address>china GuangZhou</address>
	            <age>23</age>
	            <birthday>2015-08-07 05:26:17.558 UTC</birthday>
	            <name>jack</name>
	            <sex>true</sex>
	        </com.hoo.entity.User>
	    </entry>
	</map>

http://localhost:8080/spring-mvc-json-xml-demo/jaxb2/view/doMoreXMLXStream

	<myBeans-array>
	    <myBeans>
	        <id>1</id>
	        <name>haha</name>
	        <email>email</email>
	        <address>北京</address>
	        <birthday>
	            <birthday>2010-11-22</birthday>
	        </birthday>
	    </myBeans>
	    <myBeans>
	        <id>1</id>
	        <name>haha</name>
	        <email>email</email>
	        <address>上海</address>
	        <birthday>
	            <birthday>2010-11-22</birthday>
	        </birthday>
	    </myBeans>
	</myBeans-array>

http://localhost:8080/spring-mvc-json-xml-demo/jaxb2/view/doMapXMLJaxb2

	<?xml version="1.0" encoding="UTF-8"?>
	<mapBean>
	    <map>
	        <item>
	            <key>NO2</key>
	            <value>
	                <address>china</address>
	                <birthday>
	                    <birthday>2011-11-22</birthday>
	                </birthday>
	                <email>tom@125.com</email>
	                <id>2</id>
	                <name>tom</name>
	            </value>
	        </item>
	        <item>
	            <key>NO1</key>
	            <value>
	                <address>北京</address>
	                <birthday>
	                    <birthday>2010-11-22</birthday>
	                </birthday>
	                <email>email</email>
	                <id>1</id>
	                <name>jack</name>
	            </value>
	        </item>
	    </map>
	</mapBean>

http://localhost:8080/spring-mvc-json-xml-demo/jaxb2/view/doListXMLJaxb2

	<?xml version="1.0" encoding="UTF-8"?>
	<listBean>
	    <account>
	        <address>address#0</address>
	        <birthday>
	            <birthday>2010-11-20</birthday>
	        </birthday>
	        <email>email0@120.com</email>
	        <id>1</id>
	        <name>haha#0</name>
	    </account>
	    <user>
	        <address>china GuangZhou# 0</address>
	        <age>23</age>
	        <birthday>2015-08-07T12:38:32.191+08:00</birthday>
	        <name>jack#0</name>
	        <sex>false</sex>
	    </user>
	    <account>
	        <address>address#1</address>
	        <birthday>
	            <birthday>2010-11-21</birthday>
	        </birthday>
	        <email>email1@121.com</email>
	        <id>2</id>
	        <name>haha#1</name>
	    </account>
	    <user>
	        <address>china GuangZhou# 1</address>
	        <age>24</age>
	        <birthday>2015-08-07T12:38:32.191+08:00</birthday>
	        <name>jack#1</name>
	        <sex>false</sex>
	    </user>
	    <account>
	        <address>address#2</address>
	        <birthday>
	            <birthday>2010-11-22</birthday>
	        </birthday>
	        <email>email2@122.com</email>
	        <id>3</id>
	        <name>haha#2</name>
	    </account>
	    <user>
	        <address>china GuangZhou# 2</address>
	        <age>25</age>
	        <birthday>2015-08-07T12:38:32.191+08:00</birthday>
	        <name>jack#2</name>
	        <sex>false</sex>
	    </user>
	</listBean>

http://localhost:8080/spring-mvc-json-xml-demo/jaxb2/view/doXMLJaxb2

	<?xml version="1.0" encoding="UTF-8"?>
	<account>
	    <address>address</address>
	    <birthday>
	        <birthday>2010-11-22</birthday>
	    </birthday>
	    <email>email</email>
	    <id>1</id>
	    <name>haha</name>
	</account>

----------
Version 02:
Case 01:
http://localhost:8080/spring-mvc-json-xml-demo/getUserName?name=favccxx

Case 02:
http://localhost:8080/spring-mvc-json-xml-demo/getFavUserBody

	{
		"userId": "mm",
		"userName": "女神",
		"userAge": 18,
		"createDate": "1998-08-08 18:18:18"
	}

Case 03:
http://localhost:8080/spring-mvc-json-xml-demo/buildRestUser

	{
	  "userId" : "mm",
	  "userName" : "美眉",
	  "userAge" : 18,
	  "createDate" : "2015-08-05 02:48:32"
	}


----------

Version 01

http://localhost:8080/spring-mvc-json-xml-demo/greeting?name=%E7%BE%8E%E5%A5%B3


## 参考： ##
搭建Spring MVC 4开发环境八步走

http://favccxx.blog.51cto.com/2890523/1575885

使用Spring MVC 4构建Restful服务

http://favccxx.blog.51cto.com/2890523/1576730

使用Spring RestTemplate解析RESTful服务

http://favccxx.blog.51cto.com/2890523/1579305
