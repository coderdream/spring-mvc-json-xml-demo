package com.hoo.entity;

import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.hoo.util.MapAdapter;

@XmlRootElement
public class MapBean {
    private Map<String, Object> map;

    @XmlJavaTypeAdapter(MapAdapter.class)
    public Map<String, Object> getMap() {
        return map;
    }

    public void setMap(Map<String, Object> map) {
        this.map = map;
    }
}