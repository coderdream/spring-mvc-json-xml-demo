package com.hoo.util;

import javax.xml.bind.annotation.XmlElement;

import com.hoo.entity.Account;

/**
 * <b>function:</b> MapElements
 * 
 * @author hoojo
 * @createDate 2011-4-25 下午05:04:04
 * @file MyElements.java
 * @package com.hoo.util
 * @project WebHttpUtils
 * @blog http://blog.csdn.net/IBM_hoojo
 * @email hoojo_@126.com
 * @version 1.0
 */
public class MapElements {
    @XmlElement
    public String key;

    @XmlElement
    public Account value;

    @SuppressWarnings("unused")
    private MapElements() {
    } // Required by JAXB

    public MapElements(String key, Account value) {
        this.key = key;
        this.value = value;
    }
}