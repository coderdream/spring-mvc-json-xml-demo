package com.hoo.util;

import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import com.hoo.entity.Account;

/**
 * <b>function:</b>AccountBean 编组、解组的XmlAdapter
 * 
 * @author hoojo
 * @createDate 2011-4-25 下午05:03:18
 * @file MyAdetper.java
 * @package com.hoo.util
 * @project WebHttpUtils
 * @blog http://blog.csdn.net/IBM_hoojo
 * @email hoojo_@126.com
 * @version 1.0
 */
public class MapAdapter extends XmlAdapter<MapElements[], Map<String, Account>> {
    public MapElements[] marshal(Map<String, Account> arg0) throws Exception {
        MapElements[] mapElements = new MapElements[arg0.size()];

        int i = 0;
        for (Map.Entry<String, Account> entry : arg0.entrySet())
            mapElements[i++] = new MapElements(entry.getKey(), entry.getValue());

        return mapElements;
    }

    public Map<String, Account> unmarshal(MapElements[] arg0) throws Exception {
        Map<String, Account> r = new HashMap<String, Account>();
        for (MapElements mapelement : arg0)
            r.put(mapelement.key, mapelement.value);
        return r;
    }
}
