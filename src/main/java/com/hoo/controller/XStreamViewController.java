package com.hoo.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.hoo.entity.Account;
import com.hoo.entity.Birthday;
import com.hoo.entity.User;

/**
 * <b>function:</b>
 * 
 * @author hoojo
 * @createDate 2011-4-28 下午05:58:02
 * @file JsonlibMappingViewController.java
 * @package com.hoo.controller
 * @project SpringMVC4View
 * @blog http://blog.csdn.net/IBM_hoojo
 * @email hoojo_@126.com
 * @version 1.0
 */
@Controller
@RequestMapping("/xStream/view")
public class XStreamViewController {

    /**
     * <b>function:</b>转换对象数组
     * 
     * @author hoojo
     * @createDate 2011-4-27 下午06:19:40
     * @return
     */
    @RequestMapping("/doMoreXMLXStream")
    public ModelAndView doMoreXMLXStreamView() {
        System.out.println("#################ViewController doMoreXMLXStreamView##################");
        ModelAndView mav = new ModelAndView("xStreamMarshallingView");
        Account[] accs = new Account[2];
        Account bean = new Account();
        bean.setAddress("北京");
        bean.setEmail("email");
        bean.setId(1);
        bean.setName("haha");
        Birthday day = new Birthday();
        day.setBirthday("2010-11-22");
        bean.setBirthday(day);
        accs[0] = bean;

        bean = new Account();
        bean.setAddress("上海");
        bean.setEmail("email");
        bean.setId(1);
        bean.setName("haha");
        day = new Birthday();
        day.setBirthday("2010-11-22");
        bean.setBirthday(day);
        accs[1] = bean;
        mav.addObject(accs);
        return mav;
    }
    
    /**
     * <b>function:</b>转换Map对象
     * @author hoojo
     * @createDate 2011-4-27 下午06:19:48
     * @return
     */
    @RequestMapping("/doDifferXMLXStream")
    public ModelAndView doDifferXMLXStreamView() {
        System.out.println("#################ViewController doDifferXMLXStreamView##################");
        ModelAndView mav = new ModelAndView("xStreamMarshallingView");
        
        Account bean = new Account();
        bean.setAddress("广东");
        bean.setEmail("email");
        bean.setId(1);
        bean.setName("haha");
        Birthday day = new Birthday();
        day.setBirthday("2010-11-22");
        bean.setBirthday(day);
        
        User user = new User();
        user.setAddress("china GuangZhou");
        user.setAge(23);
        user.setBirthday(new Date());
        user.setName("jack");
        user.setSex(true);
        
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("bean", bean);
        map.put("user", user);
        mav.addObject(map);
        return mav;
    }
    
    /**
     * <b>function:</b>转换List对象
     * @author hoojo
     * @createDate 2011-4-27 下午06:20:02
     * @return
     */
    @RequestMapping("/doListXMLXStream")
    public ModelAndView doListXMLXStreamView() {
        System.out.println("#################ViewController doListXMLXStreamView##################");
        ModelAndView mav = new ModelAndView("xStreamMarshallingView");
        List<Object> beans = new ArrayList<Object>(); 
        for (int i = 0; i < 3; i++) {
            Account bean = new Account();
            bean.setAddress("北京#" + i);
            bean.setEmail("email" + i + "@12" + i + ".com");
            bean.setId(1 + i);
            bean.setName("haha#" + i);
            Birthday day = new Birthday();
            day.setBirthday("2010-11-2" + i);
            bean.setBirthday(day);
            beans.add(bean);
            
            User user = new User();
            user.setAddress("china GuangZhou 广州# " + i);
            user.setAge(23 + i);
            user.setBirthday(new Date());
            user.setName("jack#" + i);
            user.setSex(Boolean.parseBoolean(i + ""));
            beans.add(user);
        }
        
        mav.addObject(beans);
        return mav;
    }
}