package com.hoo.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.hoo.entity.Account;
import com.hoo.entity.Birthday;
import com.hoo.entity.ListBean;
import com.hoo.entity.MapBean;
import com.hoo.entity.User;

/**
 * <b>function:</b>利用MarshallingView视图，配置CastorMarshaller将Java对象转换XML
 * 
 * @author hoojo
 * @createDate 2011-4-28 上午10:14:43
 * @file CastorMarshallingViewController.java
 * @package com.hoo.controller
 * @project SpringMVC4View
 * @blog http://blog.csdn.net/IBM_hoojo
 * @email hoojo_@126.com
 * @version 1.0
 */
@Controller
@RequestMapping("/castor/view")
public class CastorMarshallingViewController {

    @RequestMapping("/doBeanXMLCastorView")
    public ModelAndView doBeanXMLCastorView() {
        System.out.println("#################ViewController doBeanXMLCastorView##################");
        ModelAndView mav = new ModelAndView("castorMarshallingView");
        Account bean = new Account();
        bean.setAddress("北京");
        bean.setEmail("email");
        bean.setId(1);
        bean.setName("haha");
        Birthday day = new Birthday();
        day.setBirthday("2010-11-22");
        bean.setBirthday(day);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("day", day);
        map.put("account", bean);
        mav.addObject(bean);// 重写MarshallingView的locateToBeMarshalled方法
        // mav.addObject(BindingResult.MODEL_KEY_PREFIX, bean);
        // mav.addObject(BindingResult.MODEL_KEY_PREFIX + "account", bean);
        return mav;
    }

    @RequestMapping("/doMapXMLCastorView")
    public ModelAndView doMapXMLCastorView() {
        System.out.println("#################ViewController doMapXMLCastorView##################");
        ModelAndView mav = new ModelAndView("castorMarshallingView");
        Account bean = new Account();
        bean.setAddress("北京");
        bean.setEmail("email");
        bean.setId(1);
        bean.setName("haha");
        Birthday day = new Birthday();
        day.setBirthday("2010-11-22");
        bean.setBirthday(day);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("day", day);
        map.put("account", bean);
        MapBean differ = new MapBean();

        differ.setMap(map);

        mav.addObject(differ);
        return mav;
    }

    @RequestMapping("/doListXMLCastorView")
    public ModelAndView doListXMLCastorView() {
        System.out.println("#################ViewController doListXMLCastorView##################");
        ModelAndView mav = new ModelAndView("castorMarshallingView");
        List<Object> beans = new ArrayList<Object>();
        for (int i = 0; i < 3; i++) {
            Account bean = new Account();
            bean.setAddress("address#" + i);
            bean.setEmail("email" + i + "@12" + i + ".com");
            bean.setId(1 + i);
            bean.setName("haha#" + i);
            Birthday day = new Birthday();
            day.setBirthday("2010-11-2" + i);
            bean.setBirthday(day);
            beans.add(bean);

            User user = new User();
            user.setAddress("china GuangZhou# " + i);
            user.setAge(23 + i);
            user.setBirthday(new Date());
            user.setName("jack#" + i);
            user.setSex(Boolean.parseBoolean(i + ""));
            beans.add(user);
        }
        ListBean listBean = new ListBean();
        listBean.setList(beans);
        mav.addObject(listBean);
        return mav;
    }

    @RequestMapping("/doArrayXMLCastorView")
    public ModelAndView doArrayXMLCastorView() {
        System.out.println("#################ViewController doArrayXMLCastorView##################");
        ModelAndView mav = new ModelAndView("castorMarshallingView");
        Object[] beans = new Object[3];
        for (int i = 0; i < 2; i++) {
            Account bean = new Account();
            bean.setAddress("address#" + i);
            bean.setEmail("email" + i + "@12" + i + ".com");
            bean.setId(1 + i);
            bean.setName("haha#" + i);
            Birthday day = new Birthday();
            day.setBirthday("2010-11-2" + i);
            bean.setBirthday(day);
            beans[i] = bean;
        }
        User user = new User();
        user.setAddress("china GuangZhou# ");
        user.setAge(23);
        user.setBirthday(new Date());
        user.setName("jack#");
        user.setSex(true);
        beans[2] = user;
        mav.addObject(beans);
        return mav;
    }
}
