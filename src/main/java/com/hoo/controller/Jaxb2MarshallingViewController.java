package com.hoo.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hoo.entity.Account;
import com.hoo.entity.Birthday;
import com.hoo.entity.ListBean;
import com.hoo.entity.MapBean;
import com.hoo.entity.User;

/**
 * <b>function:</b>Jaxb2MarshallingView 视图，利用Jaxb2进行Java对象到XML的转换技术
 * 
 * @author hoojo
 * @createDate 2011-4-27 下午03:20:23
 * @file Jaxb2MarshallingViewController.java
 * @package com.hoo.controller
 * @project SpringMVC4View
 * @blog http://blog.csdn.net/IBM_hoojo
 * @email hoojo_@126.com
 * @version 1.0
 */
@Controller
@RequestMapping("/jaxb2/view")
public class Jaxb2MarshallingViewController {

    /*
     * MarshallingView Jaxb2Marshaller 需要配置转换成xml的java对象的Annotation
     */
    @RequestMapping("/doXMLJaxb2")
    public ModelAndView doXMLJaxb2View() {
        System.out.println("#################ViewController doXMLJaxb2View##################");
        ModelAndView mav = new ModelAndView("jaxb2MarshallingView");

        Account bean = new Account();
        bean.setAddress("address");
        bean.setEmail("email");
        bean.setId(1);
        bean.setName("haha");
        Birthday day = new Birthday();
        day.setBirthday("2010-11-22");
        bean.setBirthday(day);

        mav.addObject(bean);

        return mav;
    }

    /**
     * <b>function:</b>转换带有List属性的JavaBean
     * 
     * @author hoojo
     * @createDate 2011-4-27 下午05:32:22
     * @return
     */
    @RequestMapping("/doListXMLJaxb2")
    public ModelAndView doListXMLJaxb2View() {
        System.out.println("#################ViewController doListXMLJaxb2View##################");
        ModelAndView mav = new ModelAndView("jaxb2MarshallingView");
        List<Object> beans = new ArrayList<Object>();
        for (int i = 0; i < 3; i++) {
            Account bean = new Account();
            bean.setAddress("address#" + i);
            bean.setEmail("email" + i + "@12" + i + ".com");
            bean.setId(1 + i);
            bean.setName("haha#" + i);
            Birthday day = new Birthday();
            day.setBirthday("2010-11-2" + i);
            bean.setBirthday(day);
            beans.add(bean);

            User user = new User();
            user.setAddress("china GuangZhou# " + i);
            user.setAge(23 + i);
            user.setBirthday(new Date());
            user.setName("jack#" + i);
            user.setSex(Boolean.parseBoolean(i + ""));
            beans.add(user);
        }

        ListBean list = new ListBean();
        list.setList(beans);
        mav.addObject(list);

        return mav;
    }

    /**
     * <b>function:</b>转换带有Map属性的JavaBean
     * 
     * @author hoojo
     * @createDate 2011-4-27 下午05:32:42
     * @return
     */
    @RequestMapping("/doMapXMLJaxb2")
    public ModelAndView doMapXMLJaxb2View() {
        System.out.println("#################ViewController doMapXMLJaxb2View##################");
        ModelAndView mav = new ModelAndView("jaxb2MarshallingView");

        MapBean mapBean = new MapBean();

        HashMap<String, Object> map = new HashMap<String, Object>();
        Account bean = new Account();
        bean.setAddress("北京");
        bean.setEmail("email");
        bean.setId(1);
        bean.setName("jack");
        Birthday day = new Birthday();
        day.setBirthday("2010-11-22");
        bean.setBirthday(day);
        map.put("NO1", bean);

        bean = new Account();
        bean.setAddress("china");
        bean.setEmail("tom@125.com");
        bean.setId(2);
        bean.setName("tom");
        day = new Birthday("2011-11-22");
        bean.setBirthday(day);
        map.put("NO2", bean);

        mapBean.setMap(map);

        mav.addObject(mapBean);

        return mav;
    }

    /*
     * MarshallingView Jaxb2Marshaller 需要配置转换成xml的java对象的Annotation
     */
    @RequestMapping("/doXMLXStream")
    public ModelAndView doXMLXStream() {
        System.out.println("#################ViewController doXMLJaxb2View##################");
        ModelAndView mav = new ModelAndView("jaxb2MarshallingView");

        Account bean = new Account();
        bean.setAddress("address");
        bean.setEmail("email");
        bean.setId(1);
        bean.setName("haha");
        Birthday day = new Birthday();
        day.setBirthday("2010-11-22");
        bean.setBirthday(day);

        mav.addObject(bean);

        return mav;
    }

    /**
     * <b>function:</b>转换对象数组
     * 
     * @author hoojo
     * @createDate 2011-4-27 下午06:19:40
     * @return
     */
    @RequestMapping("/doMoreXMLXStream")
    public ModelAndView doMoreXMLXStreamView() {
        System.out.println("#################ViewController doMoreXMLXStreamView##################");
        ModelAndView mav = new ModelAndView("xStreamMarshallingView");
        Account[] accs = new Account[2];
        Account bean = new Account();
        bean.setAddress("北京");
        bean.setEmail("email");
        bean.setId(1);
        bean.setName("haha");
        Birthday day = new Birthday();
        day.setBirthday("2010-11-22");
        bean.setBirthday(day);
        accs[0] = bean;

        bean = new Account();
        bean.setAddress("上海");
        bean.setEmail("email");
        bean.setId(1);
        bean.setName("haha");
        day = new Birthday();
        day.setBirthday("2010-11-22");
        bean.setBirthday(day);
        accs[1] = bean;
        mav.addObject(accs);
        return mav;
    }

    /**
     * <b>function:</b>转换Map对象
     * 
     * @author hoojo
     * @createDate 2011-4-27 下午06:19:48
     * @return
     */
    @RequestMapping("/doDifferXMLXStream")
    public ModelAndView doDifferXMLXStreamView() {
        System.out.println("#################ViewController doDifferXMLXStreamView##################");
        ModelAndView mav = new ModelAndView("xStreamMarshallingView");

        Account bean = new Account();
        bean.setAddress("广东");
        bean.setEmail("email");
        bean.setId(1);
        bean.setName("haha");
        Birthday day = new Birthday();
        day.setBirthday("2010-11-22");
        bean.setBirthday(day);

        User user = new User();
        user.setAddress("china GuangZhou");
        user.setAge(23);
        user.setBirthday(new Date());
        user.setName("jack");
        user.setSex(true);

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("bean", bean);
        map.put("user", user);
        mav.addObject(map);
        return mav;
    }

    /**
     * <b>function:</b>转换List对象
     * 
     * @author hoojo
     * @createDate 2011-4-27 下午06:20:02
     * @return
     */
    @RequestMapping("/doListXMLXStream")
    public ModelAndView doListXMLXStreamView() {
        System.out.println("#################ViewController doListXMLXStreamView##################");
        ModelAndView mav = new ModelAndView("xStreamMarshallingView");
        List<Object> beans = new ArrayList<Object>();
        for (int i = 0; i < 3; i++) {
            Account bean = new Account();
            bean.setAddress("北京#" + i);
            bean.setEmail("email" + i + "@12" + i + ".com");
            bean.setId(1 + i);
            bean.setName("haha#" + i);
            Birthday day = new Birthday();
            day.setBirthday("2010-11-2" + i);
            bean.setBirthday(day);
            beans.add(bean);

            User user = new User();
            user.setAddress("china GuangZhou 广州# " + i);
            user.setAge(23 + i);
            user.setBirthday(new Date());
            user.setName("jack#" + i);
            user.setSex(Boolean.parseBoolean(i + ""));
            beans.add(user);
        }

        mav.addObject(beans);
        return mav;
    }
}