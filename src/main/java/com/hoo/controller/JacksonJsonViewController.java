package com.hoo.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.hoo.entity.Account;
import com.hoo.entity.Birthday;
import com.hoo.entity.User;

/**
 * <b>function:</b>用MappingJacksonJsonView视图和Jackson转换Json
 * 
 * @author hoojo
 * @createDate 2011-4-28 下午04:52:23
 * @file JacksonJsonViewController.java
 * @package com.hoo.controller
 * @project SpringMVC4View
 * @blog http://blog.csdn.net/IBM_hoojo
 * @email hoojo_@126.com
 * @version 1.0
 */
@Controller
@RequestMapping("/jackson/view")
public class JacksonJsonViewController {

    /**
     * <b>function:</b>转换普通Java对象
     * 
     * @author hoojo
     * @createDate 2011-4-28 下午05:14:18
     * @return
     */
    @RequestMapping("/doBeanJsonView")
    public ModelAndView doBeanJsonView() {
        System.out.println("#################ViewController doBeanJsonView##################");
        ModelAndView mav = new ModelAndView("jsonView");
        User user = new User();
        user.setAddress("china GuangZhou");
        user.setAge(23);
        user.setBirthday(new Date());
        user.setName("jack");
        user.setSex(true);

        Account bean = new Account();
        bean.setAddress("北京");
        bean.setEmail("email");
        bean.setId(1);
        bean.setName("haha");
        Birthday day = new Birthday();
        day.setBirthday("2010-11-22");
        bean.setBirthday(day);

        mav.addObject(bean);
        return mav;
    }

    /**
     * <b>function:</b>转换Map集合
     * 
     * @author hoojo
     * @createDate 2011-4-28 下午05:14:33
     * @return
     */
    @RequestMapping("/doMapJsonView")
    public ModelAndView doMapJsonView() {
        System.out.println("#################ViewController doBeanJsonView##################");
        ModelAndView mav = new ModelAndView("jsonView");
        User user = new User();
        user.setAddress("china GuangZhou");
        user.setAge(23);
        user.setBirthday(new Date());
        user.setName("jack");
        user.setSex(true);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("user", user);
        map.put("success", true);
        mav.addObject(map);
        mav.addObject("title", "ViewController doBeanJsonView");
        return mav;
    }

    /**
     * <b>function:</b>转换List集合
     * 
     * @author hoojo
     * @createDate 2011-4-28 下午05:14:54
     * @return
     */
    @RequestMapping("/doListJsonView")
    public ModelAndView doListJsonView() {
        System.out.println("#################ViewController doBeanJsonView##################");
        ModelAndView mav = new ModelAndView("jsonView");
        List<User> list = new ArrayList<User>();
        for (int i = 0; i < 3; i++) {
            User user = new User();
            user.setAddress("china GuangZhou#" + i);
            user.setAge(23 + i);
            user.setBirthday(new Date());
            user.setName("jack_" + i);
            user.setSex(true);
            list.add(user);
        }

        mav.addObject(list);
        return mav;
    }
}