package com.favccxx.favsoft.main;

import org.springframework.web.client.RestTemplate;

import com.favccxx.favsoft.favjson.pojo.FavUser;

public class ConsumeSpringRestful {

    public static void main(String[] args) {
        RestTemplate restTemplate = new RestTemplate();
        FavUser favUser = restTemplate.getForObject("http://localhost:8080/spring-mvc-json-xml-demo/buildRestUser",
                FavUser.class);
        System.out.println("userId:    " + favUser.getUserId());
        System.out.println("userName:    " + favUser.getUserName());
        System.out.println("userAge:    " + favUser.getUserAge());
        System.out.println("createDate:    " + favUser.getCreateDate());
    }
}